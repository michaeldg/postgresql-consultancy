#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 <dbname>" 1>&2
    exit 1
fi

cd /tmp

echo "Generating statistics for database $1" 1>&2
echo "Generating statistics for database $1"

echo "------------ database size -------------------------------------------"
psql -c "SELECT *, pg_size_pretty(total_bytes) AS total
    , pg_size_pretty(index_bytes) AS INDEX
    , pg_size_pretty(toast_bytes) AS toast
    , pg_size_pretty(table_bytes) AS TABLE
  FROM (
  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes FROM (
      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
              , c.reltuples AS row_estimate
              , pg_total_relation_size(c.oid) AS total_bytes
              , pg_indexes_size(c.oid) AS index_bytes
              , pg_total_relation_size(reltoastrelid) AS toast_bytes
          FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE relkind = 'r' -- AND relname IN ('batch_process','batch_item')
  ) a
) a ORDER BY table_bytes DESC ;" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ database schema dump ------------------------------------"
pg_dump -s
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_stats_bgwriter ---------------------------------------"
psql -xc "select * FROM pg_stat_bgwriter"
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_stat_all_tables --------------------------------------"
psql -xc "select * FROM pg_stat_all_tables where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by schemaname, relname" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_stat_all_indexes -------------------------------------"
psql -xc "select * FROM pg_stat_all_indexes where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by schemaname, relname" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_statio_all_tables ------------------------------------"
psql -xc "select * FROM pg_statio_all_tables where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by schemaname, relname" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_statio_all_indexes -----------------------------------"
psql -xc "select * FROM pg_statio_all_indexes where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by schemaname, relname" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_stat_all_tables percentage full table scans ----------"
psql -xc "select schemaname, relname, seq_scan, idx_scan, (seq_scan / (idx_scan+1))  AS number_nonindex_vs_index FROM pg_stat_all_tables where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') and seq_scan > 0 and schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by number_nonindex_vs_index desc" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_stat_all_tables percentage all scans ----------"
psql -xc "select schemaname, relname, (seq_scan + idx_scan) AS total_scans FROM pg_stat_all_tables where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') and seq_scan > 0 and schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by total_scans desc" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_stat_all_indexes unused indexes ----------------------"
psql -xc "select * FROM pg_stat_all_indexes where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') and idx_scan=0 order by schemaname, relname" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_statio_all_tables most disk I/O ----------------------"
psql -xc "select * FROM pg_statio_all_tables where schemaname not in ('information_schema', 'pg_toast', 'pg_catalog') order by (heap_blks_read + idx_blks_read) DESC" $1
echo "----------------------------------------------------------------------"
echo ""

echo "------------ pg_statio_all_tables heap and index hit ratio -----------"
psql -xc "CREATE TEMPORARY TABLE temp_hit_ratio AS SELECT schemaname, relname, sum(heap_blks_read) as heap_read, sum(heap_blks_hit) as heap_hit, (sum(heap_blks_hit) - sum(heap_blks_read)) / (sum(heap_blks_hit)+1) as heap_ratio, sum(idx_blks_read) as idx_read, sum(idx_blks_hit) as idx_hit, (sum(idx_blks_hit) - sum(idx_blks_read)) / (sum(idx_blks_hit)+1) as idx_ratio FROM pg_statio_user_tables WHERE schemaname NOT IN ('information_schema', 'pg_toast', 'pg_catalog') group by schemaname, relname; SELECT * FROM temp_hit_ratio order by least(heap_ratio, idx_ratio) ASC; DROP TABLE temp_hit_ratio;"
echo "----------------------------------------------------------------------"
echo ""

echo "------------ database configuration parameters -----------------------"
psql -xc 'select name,setting,category,short_desc from pg_settings'
echo "----------------------------------------------------------------------"
echo ""

echo "Done with postgres data collection, now running iostat to measure disk I/O usage" 1>&2

echo "------------ disk utilization statistics (linux only) ----------------"
iostat -x 1 3
echo "----------------------------------------------------------------------"
echo ""

echo "Done with the data collection, please send this to the consultant." 1>&2
echo "Done with the data collection, please send this to the consultant."
